import datetime
from pymongo import MongoClient
from bson.objectid import ObjectId
from sets import Set
from cache import getCache
import re

client = MongoClient()
db = client.chordmatcher

def logSimilarClicked(songId, clickedSongId):
    db.similarClickedLogs.insert_one({"source" : songId, "target" : clickedSongId, "when" : datetime.datetime.now()});
