from flask import Flask, redirect, request, Response, render_template
from queries import searchSongByNameOrArtist, searchSimilarSong, readSongFromDatabase, searchSimilarSongWithExtraChords, searchSimilarSongTransposed, searchSongsByChords
from bson.json_util import dumps
from react.render import render_component
from os import path
import json
from analytics import logSimilarClicked
from localization import getLocalizedLabels
import logging, sys

#This line enables logs to be visible in production in the wsgi logs
logging.basicConfig(stream=sys.stderr)

app = Flask(__name__)

@app.route("/")
def index():
    #This method will be called by other methods when we are supposed to return an html.
    #If it is called from there, a state attribute must be on the request, if it is not, we should initialize it as empty
    if not hasattr(request, 'state'):
        request.state = {}

    locale = getLocalizedLabels(request)
    i18nDataFile = path.realpath(path.join(path.dirname(__file__), 'static', 'i18n', locale + '.json'))
    with open(i18nDataFile, 'r') as messagesFile:
        i18nData=json.load(messagesFile)
    request.state['i18nMessages'] = {"locale" : locale, "data" : i18nData}
    filePath = path.realpath(path.join(path.dirname(__file__), 'static', 'src', 'components', "ChordMatcherApp.js"))
    rendered =  render_component(filePath, {"initialState" : request.state})
    return render_template("index.html", rendered=rendered)

@app.route("/songs")
def search():
    searchQuery = request.args.get("q")
    result = searchSongByNameOrArtist(searchQuery, getRequestPage())
    return getPagedResponse(result, "/songs?q=" + searchQuery, "searchResults");

@app.route("/chords")
def searchByChords():
    searchQuery = request.args.get("q")
    chords = searchQuery.split(" ")
    chords = sorted(chords);
    #Clean empty strings
    chords = [chord for chord in chords if chord]
    chordsFormatted = ",".join(chords)
    print chordsFormatted
    result = searchSongsByChords(chordsFormatted, getRequestPage())
    return getPagedResponse(result, "/chords/"+ searchQuery, "searchResults");

@app.route("/songs/<songId>")
def readSong(songId):
    request.state = {}
    song = readSongFromDatabase(songId);
    song['_id'] = str(song["_id"])
    request.state["song"] = song
    request.image = song["image"]
    return searchSimilar(songId);

@app.route("/songs/<songId>/similar")
def searchSimilar(songId):
    result = searchSimilarSong(songId, getRequestPage())
    return getPagedResponse(result, "/songs/"+ songId+ "/similar", "similar");

@app.route("/songs/<songId>/similarClicked/<clickedSongId>", methods=["POST"])
def similarClicked(songId, clickedSongId):
    logSimilarClicked(songId, clickedSongId)
    return ""

@app.route("/songs/<songId>/similar/extraChord")
def searchSimilarLearnChord(songId):
    result = searchSimilarSongWithExtraChords(songId, getRequestPage())
    return getPagedResponse(result, "/songs/"+ songId+ "/similar/advanced", "similar");

@app.route("/songs/<songId>/similar/transpose")
def searchSimilarTransposed(songId):
    transposeValue = int(request.args.get("transposeValue"))
    result = searchSimilarSongTransposed(songId, getRequestPage(), transposeFactor = transposeValue)
    return getPagedResponse(result, "/songs/"+ songId+ "/similar/advanced", "similar");

def getPagedResponse(result, path, key):
    page = getRequestPage()
    returnData = {"data" : result['data']}
    if 'next' in result and result['next']:
        separator = "&" if "?" in path else "?"
        returnData['next'] = path + separator +"page=" + str(page + 1)
    if(page > 0):
        separator = "&" if "?" in path else "?"
        returnData['previous'] = path + separator +"page=" + str(page - 1)
    returnData['pagesCount'] = result['pagesCount']
    return expectedResponse(returnData, key)

#Due to dynamic client side routing, our endpoints will either return json or html based on the accept header
def expectedResponse(data, key):
    if not hasattr(request, 'state'):
        request.state = {}
    if request.headers.get('Accept').split(",")[0] == "application/json":
        return Response(dumps(data), mimetype="application/json")
    else:
        request.state[key] = data
        return index()

def getRequestPage():
    page = request.args.get("page")
    if page:
        page = int(page)
    else:
        page = 0
    return page

from emailHelper import sendSupportMessage
@app.route("/support", methods = ["POST"])
def submitSupport():
    #Message will provide a beautifull 500 if it does not have all the parameters filled
    message = request.form['message']
    name = request.form['name']
    email = request.form['email']
    type = request.form['type']
    sendSupportMessage(name, email, type, message)
    return ""

if __name__ == "__main__":
    app.debug = True
    app.run()
