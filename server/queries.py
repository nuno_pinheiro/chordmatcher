from pymongo import MongoClient
from bson.objectid import ObjectId
from sets import Set
from cache import getCache
import re

client = MongoClient()
db = client.chordmatcher

PAGE_SIZE = 12

# Query helpers used in several queries
popularitySort = [("popularity" , -1)]
fieldsToReturn = {"_id" : 1, "patterns" : 1, "song" : 1, "author" : 1, "allChords" : 1, "url" : 1, "spotify_id" : 1, "image" : 1}

def searchSongByNameOrArtist(query, page):
    #this query requires a text index
    filter = {"$text" : { "$search": query}}
    returnFields = {"author" : 1, "song" : 1, "score" : { "$meta" : "textScore" }, "allChords" : 1, "patterns" : 1, "url" : 1, "spotify_id" : 1}
    #we are sorting by the score : how much  the query looks like the text. To enable this sorting we need to add the sorting to the projection fields
    sort = [( "score", { "$meta" : "textScore" } )]
    start = page * PAGE_SIZE
    #We always fetch an extra entry. If the size of the result is  higher than the page, it means we have a next page
    count = db.songs.find(filter, returnFields).sort(sort).count()
    result = db.songs.find(filter, returnFields).sort(sort).skip(start).limit(PAGE_SIZE)

    return cleanResults(result, page, count)

from bson.json_util import dumps
def searchSimilarSong(songId, page, options = None):
    if options:
        print "options aren't supported yet"
    song = db.songs.find_one({"_id" : ObjectId(songId)})
    patterns = []
    allChords = song['allChords']
    for pattern in song['patterns']:
        patterns.append(pattern['pattern'])

    #Find matches based on different queries:
    # perfectMatchFilter song has exactly the same patterns (this requires the patterns to be sorted when stored)
    # sameChordsFilter : songs with the same chords regardless of the patterns
    perfectMatchFilter = {"patternsString" : song['patternsString'], "_id" : {"$ne" : ObjectId(songId)}}
    #Algorithm caches values in the format : perfectMatches, sameChordsMatchCount
    cached = getCache().get(songId)
    if not cached:
            cached = {}
            cached['perfectMatches'] = [song for song in db.songs.find(perfectMatchFilter, fieldsToReturn)]
            perfectMatchesIds = readIds(songId, cached['perfectMatches'])
            sameChordsMatch = getSameChordsMatchQuery(allChords, skip = perfectMatchesIds)
            cached['sameChordsMatchCount'] = db.songs.find(sameChordsMatch).count()
            getCache().put(songId, cached)

    #We return PAGE_SIZE elements first fetching them from the perfect matches, and then from songs which the same chords
    matches = []
    first = page * PAGE_SIZE
    last = page * PAGE_SIZE + PAGE_SIZE;
    perfectMatches = cached['perfectMatches']
    perfectMatchesIds = readIds(songId, cached['perfectMatches'])
    perfectMatchesLen = len(perfectMatches)
    if first < perfectMatchesLen:
        matches.extend(perfectMatches[first:perfectMatchesLen])
        if(perfectMatchesLen < last):
            toFetch = last - perfectMatchesLen
            sameChordsMatches = db.songs.find(getSameChordsMatchQuery(allChords, skip = perfectMatchesIds), fieldsToReturn).sort(popularitySort).limit(toFetch)
            matches.extend([song for song in sameChordsMatches])
    else:
        #The page recieved from the browser takes into account the elements in the perfectmatches. We need to remove the size of the perfect matches to know how many we skip
        toSkip = first - perfectMatchesLen
        sameChordsMatches = db.songs.find(getSameChordsMatchQuery(allChords, skip = perfectMatchesIds), fieldsToReturn).skip(toSkip).sort(popularitySort).limit(PAGE_SIZE)
        matches.extend([song for song in sameChordsMatches])
    count = perfectMatchesLen + cached['sameChordsMatchCount']
    return cleanResults(matches, page, count)

def searchSimilarSongWithExtraChords(songId, page, extraChordsCount = 1, options = None):
    song = readSongFromDatabase(songId)
    chords = song['allChords'].split(",")
    #Transforms the chords from the format "c1,c2" to ".*c1,.*c2($|,).*"
    #This ensures that from firt to the penultimate, we enforce a comma (,) after the chords
    #On the last one we enforce either a comma or a string terminator ($)
    searchRegex = ".*" + ",.*".join(chords) + "($|,).*"
    chordsCountForResult = len(chords) + extraChordsCount
    searchQuery = { "chords_count" :  chordsCountForResult, "allChords" : {"$regex" : searchRegex}}
    count = db.songs.find(searchQuery).sort(popularitySort).count()
    start = page * PAGE_SIZE
    results = db.songs.find(searchQuery, fieldsToReturn).sort(popularitySort).skip(start).limit(PAGE_SIZE)
    return cleanResults(results, page, count)

#Returns songs transposed (for instance we could play a song with capo on 1 to play higher pitched songs)
def searchSimilarSongTransposed(songId, page, transposeFactor = 1, options = None):
    song = readSongFromDatabase(songId)
    transposedChords = transposeChords(song['allChords'].split(","), transposeFactor)
    formattedtransposedChords = ",".join(sorted(transposedChords))
    return searchSongsByChords(formattedtransposedChords, page)

def searchSongsByChords(chords, page, options = None):
    start = page * PAGE_SIZE
    searchQuery = getSameChordsMatchQuery(chords)
    count = db.songs.find(searchQuery).count()
    results = db.songs.find(searchQuery, fieldsToReturn).skip(start).limit(PAGE_SIZE)
    return cleanResults(results, page, count)

def getSameChordsMatchQuery(allChords, skip = []):
    return   {"allChords" :  allChords, "_id" : {"$nin" : skip}}

def cleanResults(result, page, count):
    data = list(result)
    for element in data:
        element["_id"] = str(element["_id"]);
    last = page * PAGE_SIZE + PAGE_SIZE
    pagesCount = count / PAGE_SIZE
    pagesCount += 1 if count % PAGE_SIZE != 0 else 0
    return {"data" : data[0:PAGE_SIZE], "next" :  last < count, "pagesCount" : pagesCount}

def readIds(songId, songs):
    ids = [ObjectId(songId)]
    for song in songs:
        ids.append(song['_id'])
    return ids

def readSongFromDatabase(songId):
    return db.songs.find_one({"_id" : ObjectId(songId)});

#TODO all the songs should be transpoed with value = 0 before going into the database to enable better matches
sharpedNotes = ["A", "A#", "B", "C", "C#", "D", "D#", "E", "F", "F#", "G", "G#"]
bemoledNotes = ["A", "Bb", "B", "C", "Db", "D", "Eb", "E", "F", "Gb", "G", "Ab"]
def transposeChords(chords, value):
    transposedChords = []
    for chord in chords:
        chordRegex = "([A-G][#b]?)(.*)"
        chordParts = re.match(chordRegex, chord).groups()
        chordBase = chordParts[0]
        transposedChord = transposeChord(chordBase, value) + chordParts[1]

        #Check if is a chord with other bass position (e.g Am/G)
        if "/" in transposedChord:
            splitted = transposedChord.split("/")
            trasnposedBass = transposeChord(splitted[1], value)
            transposedChord = splitted[0] + "/" + trasnposedBass
        transposedChords.append(transposedChord)
    return transposedChords

def transposeChord(chord, value):
    #Since we are transposing up and down, we need to get the transposed index based on module 12
    if chord in sharpedNotes:
        index = sharpedNotes.index(chord)
        transposedIndex = (index + value) % 12
        sharpedNotes[transposedIndex]
    elif chord in bemoledNotes:
        index = bemoledNotes.index(chord)
        transposedIndex = (index + value) % 12
        bemoledNotes[transposedIndex]
    else:
        raise ValueError('Chord ' + chord + " is invalid!")
    return


#write tests in here
if __name__ == "__main__":
    print transposeChords(["C", "Am/G", "Dbm", "A7"], 0)
    print transposeChords(["C", "Cm", "Ab7"], 0)
