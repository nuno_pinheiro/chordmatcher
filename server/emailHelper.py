import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.header import Header
from properties import emailPassword
def sendSupportMessage(name, email, type, message):
    body = """\
    User name: %s
    User e-mail: %s
    Type : %s
    Message : \n
    %s
    """ % (name, email, type, message)
    sendEmail("[ChordMatcher] User Feedback", body, "nmgfpinheiro@gmail.com")

def sendEmail(subject, body, to):
    msg = MIMEMultipart('alternative')
    msg.set_charset('utf8')
    msg['FROM'] = "ChordMatcher"
    msg['TO'] = to
    msg = MIMEText(body, _charset="UTF-8")
    msg['Subject'] = Header( subject.encode('utf-8'), 'UTF-8').encode()
    server = smtplib.SMTP_SSL('smtp.gmail.com:465')
    server.login("chordmatcher@gmail.com", emailPassword)
    server.sendmail("chordmatcher@gmail.com", to, msg.as_string())
    return ""
