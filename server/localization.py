supportedLocales = ['pt-PT', 'en-GB']
supportedLanguages = dict([(locale.split("-")[0], locale) for locale in supportedLocales])

def getLocalizedLabels(request):
    locale = "en-GB"
    if "preferredLocale" in request.cookies and request.cookies['preferredLocale'] in supportedLocales:
        locale = request.cookies['preferredLocale']
    elif "Accept-Language" in request.headers:
        locales = languageGenerator(request.headers['Accept-Language'])
        for acceptLanguage in locales:
            if acceptLanguage in supportedLocales:
                locale = acceptLanguage
                break
            elif acceptLanguage in supportedLanguages.keys():
                #The accept header had a language instead of a locale
                locale = supportedLanguages[acceptLanguage]
                break
    return locale

def languageGenerator(acceptHeader):
    for accept in acceptHeader.split(";"):
        for acceptLanguage in accept.split(","):
            if not acceptLanguage[0:2] == "q=":
                yield acceptLanguage

def handleAccepptLanguage(language):
    print i


class requestFacade():
    def     __init__(self, headers, cookies):
        self.headers = headers
        self.cookies = cookies

if __name__ == "__main__":
    test1 = requestFacade({"Accept-Language" : "en-US,en;q=0.8,pt;q=0.6"}, {})
    test2 = requestFacade({"Accept-Language" : "pt,en-US;q=0.8,en;q=0.6"}, {})
    test3 = requestFacade({"Accept-Language" : "en-US,en;q=0.8,pt;q=0.6"}, {"preferredLocale" : "pt-PT"})
    test4 = requestFacade({"Accept-Language" : "en-US,en;q=0.8,pt;q=0.6"}, {"preferredLocale" : "fr-FR"})
    #expect en-GB
    print getLocalizedLabels(test1)
    #expect pt-PT
    print getLocalizedLabels(test2)
    #expect pt-PT
    print getLocalizedLabels(test3)
    #expect en-GB
    print getLocalizedLabels(test4)
