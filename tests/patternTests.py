import unittest
from scrapping.patternfinder.finder import detectPattern, patternsListAsString;

class TestPatternFinder(unittest.TestCase):

    def test_hotel_california(self):
        """Hotel California Chords detection """

        chords =  [ "Bm", "Fm", "A", "E", "G", "D", "Em", "Fm",
                    "Bm", "Fm", "A", "E", "G", "D", "Em", "Fm",
                    "Bm", "Fm", "A", "E", "G", "D", "Em", "Fm",
                    "G", "D", "E", "Fm", "G", "D", "E", "Fm",
                    "Bm", "Fm", "A", "E", "G", "D", "Em", "Fm",
                    "Bm", "Fm", "A", "E", "G", "D", "Em", "Fm",
                    "Bm", "Fm", "A", "E", "G", "D", "Em", "Fm",
                    "Bm", "Fm", "A", "E", "G", "D", "Em", "Fm" ];


        expectedPatterns = [{'count': 2, 'pattern': ['G', 'D', 'E', 'Fm']},
                            {'count': 7, 'pattern': ['Bm', 'Fm', 'A', 'E', 'G', 'D', 'Em', 'Fm']}]


        detectedPatterns = detectPattern(chords)

        self.assertEqual(detectedPatterns, expectedPatterns)

    def test_patterns_as_list_is_sorted(self):
        ''' Parameters as list must be sorted '''

        patterns = [{'count': 2, 'pattern': ['G', 'D', 'E', 'Fm']},
                            {'count': 7, 'pattern': ['Bm', 'Fm', 'A', 'E', 'G', 'D', 'Em', 'Fm']}]
        song = {"patterns" : patterns}
        self.assertEqual(patternsListAsString(song['patterns']), "Bm,Fm,A,E,G,D,Em,Fm;G,D,E,Fm")

if __name__ == '__main__':
    unittest.main()
