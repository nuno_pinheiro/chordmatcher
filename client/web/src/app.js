import ReactDOM from 'react-dom';
import React from 'react';
import ChordMatcherApp from './components/ChordMatcherApp';

var App = React.createFactory(ChordMatcherApp);
ReactDOM.render( App({initialState : initialState}), document.getElementById('content'));
