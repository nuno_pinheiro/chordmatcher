var SocialTriggerMixin = {
  componentDidMount : function(){
    //We need to check if FB is already defined, if it is not it will be fetched after our component is on the DOM
    if(typeof FB !== "undefined"){
      window.setTimeout(function(){
        // Classic hack. It seems that during this phase the browser still hasnt the dom.
        // We can only call the parse method after the thread is released
        FB.XFBML.parse();
        twttr.widgets.load();
      }, 0);
    }
  }
};

export default SocialTriggerMixin;
