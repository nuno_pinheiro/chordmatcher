import React from "react";

var ChordComponent = React.createClass({
  render: function() {
    return (
      <span>
        {this.props.chord}
      </span>
    );
  }
});

export default ChordComponent;
