import React from "react";
import { createStore } from 'redux';
import { Layout, Header, Content, Footer, FooterSection, FooterLinkList } from 'react-mdl';
import reducer from "../reducers/Reducer";
import ResultsList from "./ResultsList";
import SongComponent from "./SongComponent";
import SearchBox from "./SearchBox";
import SupportForm from "./SupportForm";
import I18NSelectionComponent from "./I18NSelectionComponent";
import { i18nEventsListenerMixin, setupI18NConfiguration, changeLocale, i18n } from "react-i18n-helper";


var ChordMatcherApp = React.createClass({
  mixins : [i18nEventsListenerMixin],
  getInitialState : function(){
    var initialSearchBoxStatus = {searchType : "song", searchValue : ""};
    var state = this.props.initialState || {searchResults : {}, similarSongs : {}, song : {}};

    //support form state always starts as closed
    state.supportForm = false;
    if(typeof(this.props.initialState) == "string"){
      state = JSON.parse(state);
    }
    //Update the state of the application to enable future back navigation
    if (typeof history !== 'undefined') {
      history.replaceState(state, "");
    }
    setupI18NConfiguration({serverPath : "../../i18n", clientPath : "/static/i18n", fallBackLocale : "en-GB", initialState : state.i18nMessages});
    return {store : createStore(reducer, state)};
  },
  componentDidMount : function(){
    window.onpopstate = this.handleBackEvent;
    this.state.store.subscribe(this.updateView);
    // HACK : Safari triggers a back event when the app starts.
    // We need to avoid this event the first time it happens in safari :\
    var isSafari = navigator.vendor.indexOf("Apple")===0 && /\sSafari\//.test(navigator.userAgent);
    this.ignoreNextBackCall = isSafari;
  },
  render : function(){
      var storeState = this.state.store.getState();
      var currentView = null;
      if(!storeState.song && !storeState.searchResults){
        currentView = [
                       <Content key="content">
                          <div className="background-image"> </div>
                          <div key="overlay" id="color-overlay"></div>
                          <div style={{color : "white", textAlign : "center"}}>
                            <h1>ChordMatcher</h1>
                            <h2>{i18n("motto")}</h2>
                            <p>{i18n("explanation")}</p>
                            <SearchBox style={{margin : "0 auto", width : "400px", borderRadius : "10px"}} className="mdl-color--primary" store={this.state.store}/>
                          </div>
                       </Content>];
      }
      else{
        if(storeState.song){
          currentView = <Content>
                          <SongComponent store={this.state.store} />
                        </Content>;
        }
        else{
          currentView = <Content>
                          <ResultsList store={this.state.store} title="Song search" store_data="searchResults"/>
                        </Content>;
        }
      }
      var supportForm = null;
      if(this.state.supportForm){
        supportForm = <SupportForm onClose={this.toggleSupportForm}/>;
      }
      return (<div><Layout style={{}} fixedHeader>
                <Header title="ChordMatcher" style={{color: 'white'}} scroll>
                  <SearchBox className="desktop-search" store={this.state.store}/>
                  <I18NSelectionComponent />
                </Header>
                <SearchBox className="mobile-search" store={this.state.store}/>
                {currentView}
                <Footer size="mini">
                <FooterSection type="left">
                  <div>
                    ChordMatcher™
                  </div>
                </FooterSection>
                  <FooterSection type="right">
                       <FooterLinkList>
                           {
                             //i18n ignored for now since footer is not decided yet
                           }
                           <a href="javascript:void(0)" onClick={this.toggleSupportForm}>{i18n("supportForm")}</a>
                       </FooterLinkList>
                  </FooterSection>
                </Footer>
              </Layout>
              {supportForm}
              </div>
            );
  },
  updateView : function(){
    this.setState({});
  },
  toggleSupportForm : function(){
    var newSupportFormState = !this.state.supportForm;
    this.setState({ supportForm : newSupportFormState});
  },
  handleBackEvent : function(){
    if(this.ignoreNextBackCall){
      //Avoid first back event on safari
      this.ignoreNextBackCall = false;
      return;
    }
    this.state.store.dispatch({ type : "back"});
  }
});


export default ChordMatcherApp;
