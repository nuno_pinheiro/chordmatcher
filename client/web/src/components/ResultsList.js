import React from "react";
import {Button, Card, CardText, CardTitle, CardActions, Grid} from 'react-mdl';
import ChordComponent from './ChordComponent';
import { i18nEventsListenerMixin, i18n } from "react-i18n-helper";

var ResultsList = React.createClass({
  mixins : [i18nEventsListenerMixin],
  getInitialState : function() {
    return {elements : [], page : 0};
  },
  componentDidMount : function(){
    this.state.unsubscribe = this.props.store.subscribe(this.processChangeEvent);
    if(this.props.store.getState()[this.props.store_data]){
      this.processChangeEvent();
    }
  },
  componentWillUnmount: function(){
    this.state.unsubscribe();
  },
  render : function(){
    if(this.state.elements.length === 0){
      return <div>{i18n("noResults")}</div>;
    }
    var searchResults = this.state.elements.map(function(entry) {
      var chords = entry.allChords.split(",").map(this.mapChords);
      return (
        <Card shadow={0} key={entry._id} className="mdl-cell--4-col" style={{minHeight : "0px", "marginLeft" : "12px", "marginTop" : "12px"}}>
          <CardText border>
            <CardTitle>
              {entry.song}
            </CardTitle>
            {i18n("by")} {entry.author}
            <div>
              {i18n("chords")} : {chords}
            </div>
          </CardText>
          <CardActions border>
            <Button raised colored onClick={this.selectedSong(entry)}>{i18n("seeSimilar")}</Button>
          </CardActions>
        </Card>
      );
    }.bind(this));

    var more = null;
    if(this.state.next){
      var morePages = this.state.pages - (this.state.elements.length / 12);
      more = <div style={{marginTop :"12px", marginBottom : "12px", marginLeft : "12px"}}>
                <Button raised onClick={this.fetchMore}>{i18n("more")}</Button>
                ({i18n("more")} {morePages} {i18n("pages")})
              </div>;
    }
    return  <div style={{background : "#DDDDDD"}}>
              <Grid>
                {searchResults}
                {more}
              </Grid>
            </div>;

  },
  processChangeEvent : function(event){
    var data = this.props.store.getState()[this.props.store_data];
    if(data){
      this.setState({next : data.next, previous : data.previous, pages : data.pagesCount, elements : data.data || []});
    }else{
      this.setState(this.getInitialState());
    }
  },
  mapChords : function(chord, index){
    return(
      <ChordComponent key={index} chord={chord} />
    );
  },
  fetchMore : function(){
    $.get(this.state.next, this.processResponse, "json");
  },
  processResponse : function(data){
    this.props.store.dispatch({ type : this.props.store_data, data: data, operation : "add"});
  },
  selectedSong : function(song){
    return function(event){
      event.preventDefault();
      if(this.props.clickEventServerNotifier){
        this.props.clickEventServerNotifier(song._id);
      }
      this.props.store.dispatch({ type : "songSelected", data: song});
      history.pushState(this.props.store.getState(), "", `/songs/${song._id}`);
    }.bind(this);
  }
});

export default ResultsList;
