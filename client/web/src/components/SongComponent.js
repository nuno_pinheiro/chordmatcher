import React from "react";
import { Tooltip, Icon, FABButton, Button } from "react-mdl";
import ResultsList from './ResultsList';
import ChordComponent from './ChordComponent';
import { i18nEventsListenerMixin, i18n } from "react-i18n-helper";
import SocialTriggerMixin from "./SocialTriggerMixin";

var SongComponent = React.createClass({
  mixins : [i18nEventsListenerMixin, SocialTriggerMixin],
  getInitialState : function(){
    return {song : null, calculatingSongs : false};
  },
  render : function(){
    if(!this.state.song || Object.keys(this.state.song).length === 0){
      return null;
    }
    var song = this.state.song;
    var chords = song.allChords.split(",").map(function(chord, index){
      return (<ChordComponent key={index} chord={chord} />);
    });

    var transposeOptions = [];
    //We only add from fret 1 to 11. Then it repeats
    for(var i = 1; i < 12; i++){
      transposeOptions.push(<option key={i} value={i}>{i}</option>);
    }
    var spotifyView = null;
    if(song.spotify_id !== ""){
      var url = `https://embed.spotify.com/?uri=spotify:track:${song.spotify_id}&theme=white&view=coverart`;
      spotifyView = <iframe src={url} width="300" height="90" frameborder="0" allowtransparency="true"></iframe>;
    }
    var advancedSearch = null;
    if(this.state.advancedSearch){
      advancedSearch = <div style={{padding : "12px"}}>
                          <Tooltip label={<span>{i18n("learnNewChordHelp")}</span>}>
                            <Button raised colored onClick={this.fetchSimilarWithExtraChord}>{i18n("learnNewChord")}</Button>
                          </Tooltip>
                          <div>
                          <Tooltip label={<span>{i18n("transposeHelp")}</span>}>
                            <select style={{marginTop : "12px"}} className="mdl-button mdl-button--raised mdl-button--colored" onChange={this.fetchSimilarTransposed}>
                              <option>{i18n("none")}</option>
                              {transposeOptions}
                            </select>
                          </Tooltip>
                          </div>
                      </div>;
    }
    return(
      <div>
        <div style={{"marginLeft":" 24px","marginBottom": "12px", "marginRight":"24px"}}>
            <div style={{"float":"right"}}>
              {spotifyView}
              <Button onClick={this.gotoChordsPage} style={{"backgroundColor":"black"," color":"white", "marginTop" : "12px", color : "white", display : "block"}}>{i18n("chordsPage")}</Button>
              <div style={{marginTop : "12px"}}>
                <div style={{marginRight : "12px"}} className="fb-share-button" data-href={ "http://chordmatcher.com/songs/" + song._id} data-layout="button" data-mobile-iframe="true"></div>
                <a href="https://twitter.com/share" className="twitter-share-button" data-via="chordmatcher" data-hashtags="chordmatcher">Tweet</a>
              </div>
              <div style={{fontSize : "30px"}}>
                {i18n("advancedSearch")}
                <FABButton style={{marginLeft : "6px"}} mini  onClick={this.toggleAdvancedSearch}><Icon name="keyboard_arrow_down"/></FABButton>
              </div>
            </div>
            <div style={{lineHeight : "45px", fontSize : "25px", "marginBottom": "36px"}}>
              <h1>{song.song}</h1>
              <div>{i18n("by")}{song.author}</div>
              <div>{i18n("chords")} : {chords}</div>
            </div>
            <div style={{backgroundColor : "#DDDDDD"}}>
              {advancedSearch}
            </div>
        </div>
        <ResultsList store={this.props.store} store_data="similarSongs" clickEventServerNotifier={this.notifySimilarSongClick}/>
      </div>
    );
  },
  componentDidMount : function(){
    this.state.unsubscribe = this.props.store.subscribe(this.songChanged);
    if(this.props.store.getState().song){
      this.songChanged();
    }
  },
  componentWillUnmount: function(){
    this.state.unsubscribe();
  },
  songChanged : function(){
    var newSong = this.props.store.getState().song;
    var calculatingSongs = this.state.song != newSong;
    if(newSong && calculatingSongs){
      this.fetchSimilarSongs(newSong);
    }
    this.setState({song : newSong, calculatingSongs : calculatingSongs});
  },
  fetchSimilarSongs : function(song){
    $.get("/songs/" + song._id + "/similar", {}, this.processSimilarFetch, "json");
  },
  processSimilarFetch : function(data){
    this.setState({calculatingSongs : false});
    this.props.store.dispatch({ type : "similarSongs", data: data});
  },
  fetchSimilarWithExtraChord : function(){
    this.setState({calculatingSongs : true});
    $.get("/songs/" + this.state.song._id + "/similar/extraChord", {}, this.processSimilarFetch, "json");
  },
  fetchSimilarTransposed : function(event){
    var transposeValue = event.target.value;
    if(!transposeValue){
      //Turn off transpose
      this.fetchSimilarSongs();
      return;
    }
    this.setState({calculatingSongs : true});
    var url = "/songs/" + this.state.song._id + "/similar/transpose?transposeValue=" + transposeValue;
    $.get(url, {}, this.processSimilarFetch, "json");
  },
  gotoChordsPage : function(){
    window.open(this.state.song.url);
  },
  toggleAdvancedSearch : function(){
    this.setState({advancedSearch : !this.state.advancedSearch});
  },
  notifySimilarSongClick : function(songId){
    //Tell the server that a similar song was chosen from this song
    $.post(`/songs/${this.state.song._id}/similarClicked/${songId}`);
  }
});

export default SongComponent;
