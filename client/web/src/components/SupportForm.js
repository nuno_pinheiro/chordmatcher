import React from "react";
import { i18nEventsListenerMixin, i18n } from "react-i18n-helper";
import { Snackbar } from "react-mdl";

var emailRegex = /^[-a-z0-9~!$%^&*_=+}{\'?]+(\.[-a-z0-9~!$%^&*_=+}{\'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$/i;

var SupportForm = React.createClass({
  mixins : [i18nEventsListenerMixin],
  getInitialState : function(){
    return {name : "", email :"", type : "", message :"", toastMessage : null};
  },
  render : function(){
    return (
      <div className="supportForm">
        <Snackbar active={ this.state.toastMessage !== ""} onTimeout={() => true}>
                { this.state.toastMessage }
        </Snackbar>;
        <div className="form">
          <fieldset>
            <label for="name">{i18n("name")}</label>
            <input type="text" name="name"
                  onChange={(event) => this.handlePropertyChange("name", event)}
                  value={this.state.name}/>
          </fieldset>

          <fieldset>
            <label for="email">{i18n("email")}</label>
            <input type="text" name="email"
                onChange={(event) => this.handlePropertyChange("email", event)}
                value={this.state.email}/>
          </fieldset>

          <fieldset>
            <label for="type">{i18n("type")}</label>
            <select name="type"
                onChange={(event) => this.handlePropertyChange("type", event)}
                value={this.state.type}>
              <option value=""></option>
              <option value="suggestion">{i18n("suggestion")}</option>
              <option value="error">{i18n("error")}</option>
            </select>
          </fieldset>

          <fieldset>
            <label for="textarea">{i18n("message")}</label>
            <textarea name="textarea" placeholder={i18n("messagePlaceholder")}
                value={this.state.message}
                onChange={(event) => this.handlePropertyChange("message", event)}>
                </textarea>
          </fieldset>

        <button class="send" onClick={this.submitSupport}><i class="fa fa-paper-plane-o"></i>{i18n("send")}</button>
				<button class="cancel" onClick={() => this.props.onClose()}>{i18n("cancel")}</button>

        </div>
      </div>);
  },
  handlePropertyChange : function(property, event){
    var newPropertyState = {toastMessage : ""};
    newPropertyState[property] = event.target.value;
    this.setState(newPropertyState);
  },
  submitSupport : function(){
    var toastMessage = "";
    if(this.state.name === ""){
      toastMessage += i18n("nameEmpty");
    }
    if(this.state.email === ""){
      toastMessage += i18n("emailEmpty");
    }
    else if(!this.state.email.match(emailRegex)){
      toastMessage += i18n("emailInvalid");
    }
    if(this.state.type === ""){
      toastMessage += i18n("typeEmpty");
    }
    if(this.state.message === ""){
      toastMessage += i18n("messageEmpty");
    }
    if(toastMessage !== ""){
      this.setState({toastMessage : toastMessage});
    }
    else{
      delete this.state.toastMessage;
      $.post("support", this.state)
       .done(() => this.props.onClose())
       .fail(() => this.setState({toastMessage : i18n("errorSending")}));
    }
  }
});

export default SupportForm;
