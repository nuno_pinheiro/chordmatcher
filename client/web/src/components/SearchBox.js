import React from "react";
import Autocomplete from 'react-autocomplete/build/lib/';
import {Button, Textfield} from 'react-mdl';
import { styles } from 'react-autocomplete/build/lib/utils';
import { chords } from '../auto-complete-chords';
import { i18nEventsListenerMixin, i18n } from "react-i18n-helper";

var SearchBox = React.createClass({
  mixins : [i18nEventsListenerMixin],
  getInitialState: function() {
    return {value : "", searchType : "song"};
  },
  componentDidMount : function(){
    var search = window.location.search;
    if(search.indexOf("?q=") === 0){
      this.setState({value : search.replace("?q=", "")});
    }
    this.unsubscribe = this.props.store.subscribe(this.updateStateFromStore);
  },
  componentWillUnmount: function(){
    this.unsubscribe();
  },
  updateStateFromStore : function(){
    var newState = this.props.store.getState().searchBoxStatus || {};
    this.setState(newState);
  },
  valueChanged : function(e){
    var newState = this.state;
    newState.value = e.target.value;
    this.props.store.dispatch({type : "searchBoxChange", data : newState});
  },
  processButton : function(e){
    if(e.key == "Enter"){
      this.submit();
    }
  },
  submit : function(){
    if(this.state.searchType == "song"){
      history.pushState(this.props.store.getState(), "", `/songs?q=${this.state.value}`);
      $.get("/songs", {q : this.state.value}, this.processResponse, "json");
    }
    else{
      history.pushState(this.props.store.getState(), "", `/chords?q=${this.state.value}`);
      $.get("/chords", {q : this.state.value}, this.processResponse, "json");
    }
  },
  processResponse : function(data){
    this.props.store.dispatch({ type : "searchResults", data: data});
  },
  searchTypeChanged : function(event){
    var newSearchType = this.state.searchType == "song" ?  "chords" : "song";
    var newSearchState = {searchType : newSearchType, value : ""};
    this.setState(newSearchState);
    this.props.store.dispatch({type : "searchBoxChange", data : newSearchState});
  },
  match : function(value, state){
    var parts = state.split(" ");
    var latest = parts[parts.length -1];
    if(latest === ""){
      return;
    }
    return value.toUpperCase().indexOf(latest.toUpperCase()) === 0;
  },
  autoCompleteSelection : function(value){
    //When a chord is selected from autocomplete we remove the last part of the current string
    var parts = this.state.value.split(" ");
    var newValue = "";
    for(var i = 0; i < parts.length -1; i++){
      newValue += parts[i] + " ";
    }
    newValue += value;
    this.setState({value : newValue});
  },
  handleAutoCompleteKeyPress: function(event){
    if(event.key === 'Enter'){
      console.log("here");
    }
  },
  render: function() {
    var input = null;
    if(this.state.searchType == "song"){
      input = <Textfield type="text" label={i18n("bySong")} name="value" value={this.state.value} onChange={this.valueChanged} onKeyDown={this.processButton}/>;
    }else{
      input = <Autocomplete
        onKeyDown={this.processButton}
        value={this.state.value}
        inputProps={{className : "mdl-textfield__input", onKeyPress: this.handleAutoCompleteKeyPress, placeholder : i18n("byChords")}}
        wrapperProps={{className : "mdl-textfield mdl-js-textfield is-upgraded"}}
        items={chords}
        getItemValue={(item) => item}
        onChange={this.valueChanged}
        shouldItemRender={this.match}
        sortItems={(a, b) => a.length > b.length ? 1 : -1}
        onSelect={this.autoCompleteSelection}
        renderItem={(item, isHighlighted) => (
          <div style={isHighlighted ? styles.highlightedItem : this.styles.automCompleteItem} key={item}>{item}</div>
        )}
      />;
    }
    return (
      <div className={this.props.className} style={this.props.style}>
        <button className="mdl-button  mdl-button--icon mdl-js-button" onClick={this.searchTypeChanged}><i className="material-icons">transform</i></button>
        {input}
        <Button type="submit" onClick={this.submit}>
          <i className="material-icons">search</i>
        </Button>
      </div>
    );
  },
  styles : {
    automCompleteItem : {
      backgroundColor : "black",
      color : "white"
    }
  }
});

export default SearchBox;
