import React from "react";
import {changeLocale} from "react-i18n-helper";
import Cookies from "js-cookie";

var I18NSelectionComponent = React.createClass({
  render: function() {
    return (
      <div className="language-flags">
        <img onClick={() => this.changeLocale("pt-PT")} src="/static/i18n/pt.svg" alt="pt-PT" />
        <img onClick={() => this.changeLocale("en-GB")} src="/static/i18n/gb.svg" alt="en-GB" />
      </div>
    );
  },
  changeLocale : function(locale){
    Cookies.set('preferredLocale', locale);
    changeLocale(locale);
  }
});

export default I18NSelectionComponent;
