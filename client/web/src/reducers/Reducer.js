var reducer = function(state, event){
  if(event.type == "back"){
    state = history.state;
  }
  if(event.type == "searchBoxChange"){
    state.searchBoxStatus = event.data;
  }
  if(event.type == "searchResults"){
    if(event.operation == "add"){
      state.searchResults.data = state.searchResults.data.concat(event.data.data);
      state.searchResults.next = event.data.next;
      console.log(event.data.data.next);
    }
    else{
      state.searchResults = event.data;
    }
    state.similarSongs = {};
    state.song = null;
  }
  if(event.type == "similarSongs"){
    if(event.operation == "add"){
      state.similarSongs.data = state.similarSongs.data.concat(event.data.data);
      state.similarSongs.next = event.data.next;
      console.log(event.data.data.next);
    }
    else{
      state.similarSongs = event.data;
    }
    state.searchResults = [];
  }
  if(event.type == "songSelected"){
    state.song = event.data;
    state.searchResults = [];
  }
  return state;
};

export default reducer;
