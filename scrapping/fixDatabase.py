from pymongo import MongoClient
from Queue import Queue
import patternfinder.finder as patternfinder
from sets import Set
from chordCleaner import cleanChord
#Script to fix the patterns on current database entries

client = MongoClient()
db = client.chordmatcher

threads = []


def readChordsFromPatterns(patterns):
    chords = Set()
    for pattern in patterns:
        for chord in pattern['pattern'].split(','):
            chords.add(chord.strip())
    sortedList = ",".join(sorted(list(chords)))
    try:
        return str(sortedList)
    except:
        print patterns
        return ""

for song in db.songs.find().batch_size(5000):
    #song['patterns'] = patternfinder.detectPattern(song['chords'])

    #song['patternsString'] = patternfinder.patternsListAsString(song['patterns'])

    #for pattern in song['patterns']:
    #    pattern['pattern'] = patternfinder.getPatternAsString(pattern['pattern'])

    #song['allChords'] = readChordsFromPatterns(song['patterns'])
    #song['chords_count'] = len(song['allChords'].split(","))
    chords =  song['allChords'].split(",")
    chords = [cleanChord(chord) for chord in chords]
    song['allChords'] = ",".join(chords)
    try:
        db.songs.update({"_id" : song["_id"]}, song)
    except:
        print "cant write " +  song['author']  + " " +  song['song']
