from datetime import datetime
from datetime import timedelta
from collections import defaultdict
from collections import OrderedDict
from collections import deque

#Some times we find some patterns which are the same, although we found them in the wrong order
#We will try to keep them anchored on the first chord of the music since it is usually the start of the pattern (unless the music started with "this music uses these chords")
def matchRotation(musicFirstChord, pattern1, pattern2):
    if len(pattern1) != len(pattern2):
        return False
    firstChord = pattern1[0]
    for i in range(0, len(pattern2)):
        if pattern2[i] == firstChord:
            pattern1Rotated = pattern1[-i:] + pattern1[:-i]
            if pattern1Rotated == pattern2:
                if pattern2[0] == musicFirstChord:
                    return pattern2
                else:
                    return pattern1


def detectPattern(chords):
    start = datetime.now()

    patterns = []
    i = 0
    lastNonPattern = -2
    nonPatterns = []
    while i <  len(chords):
        found = False
        for j in range(i + 1, len(chords)):
            patternLen = j - i + 1
            #we are matching patterns with length of at least 2
            pattern = chords[i:j+1]
            nextMatchEnd = j + patternLen
            while pattern == chords[j+1:nextMatchEnd + 1]:
                patterns.append(pattern)
                # Since we found a pattern, we can check if the pattern repeats in the following chords
                i = nextMatchEnd + 1
                j += patternLen
                nextMatchEnd += patternLen
                found = True
            if found:
                #When we found a pattern due to a match, we found it at least twice (the pattern, and the matching) but we only added one per match, add the initial one here
                patterns.append(pattern)
                break
        #We still need to store "non-patterns". For instance, a chorus may be played twice in a music, and the previous algorithm won't find it
        if not found:
            if lastNonPattern == i-1:
                latestEntry = len(nonPatterns) - 1
                newNonPattern = nonPatterns[latestEntry] + [chords[i]]
                lastPattern = patterns[len(patterns) - 1] if len(patterns) > 0  else []
                #if this "non-pattern" equals the latest found pattern, it is not a non-pattern :\
                # We need this code because we avoided "double jumping" patterns
                if newNonPattern == lastPattern:
                    del nonPatterns[latestEntry]
                    lastNonPattern = -2
                    patterns.append(lastPattern)
                else:
                    nonPatterns[latestEntry] = nonPatterns[latestEntry] + [chords[i]]
                    lastNonPattern  = i
            else:
                nonPatterns.append([chords[i]])
                lastNonPattern  = i
            i +=1

    #Add all non patterns to the patterns
    patterns += nonPatterns

    measuredPatterns = defaultdict(int)
    patternsFromKeys = {}
    for pattern in patterns:
        stringified = str(pattern)
        patternsFromKeys[stringified] = pattern
        measuredPatterns[stringified] = measuredPatterns[stringified] + 1
    keys = patternsFromKeys.keys()

    for i in range(0,len(keys)):
        skip = False
        if measuredPatterns[keys[i]] == 0:
            continue
        for j in range(i+1,len(keys)):
            if measuredPatterns[keys[j]] == 0:
                #avoid comparing if any has 0. That would mean it was already cleared
                continue
            patternI = patternsFromKeys[keys[i]]
            patternJ = patternsFromKeys[keys[j]]
            match = matchRotation(chords[0], patternI, patternJ)
            if match == patternI:
                measuredPatterns[keys[i]] += measuredPatterns[keys[j]]
                measuredPatterns[keys[j]] = 0
            if match == patternJ:
                measuredPatterns[keys[j]] += measuredPatterns[keys[i]]
                measuredPatterns[keys[i]] = 0
                break


    measuredPatterns = OrderedDict(sorted(measuredPatterns.items(), key=lambda x:x[1]))
    nonEmptyPatterns = []
    for entry in  measuredPatterns.keys():
        if measuredPatterns[entry] != 0:
            nonEmptyPatterns.append({"pattern" : patternsFromKeys[entry], "count" : measuredPatterns[entry]})
    return nonEmptyPatterns

def patternsListAsString(patterns):
    patternsAsStrings = []
    for pattern in patterns:
        patternAsString = getPatternAsString(pattern['pattern'])
        patternsAsStrings.append(patternAsString)
    patternsString = ";".join(sorted(patternsAsStrings))
    return patternsString

def getPatternAsString(pattern):
    return ",".join(pattern)
