from http.helper import get 
from HTMLParser import HTMLParser
import re

class Scrapper:
    def scrap(self, url):
       data = get(url)
       parser = UltimateGuitarChordParser()
       parser.feed(data)
       return {"chords" : parser.chords , "capo" : parser.capo, "tuning" : parser.tuning}

class UltimateGuitarChordParser(HTMLParser):
    def __init__(self):
        HTMLParser.__init__(self)
        self.currentLevel = 0
        self.tabContentLevel = None
        self.inChord = False
        self.chords = []
        self.capo = None
        self.tuning = None
    def handle_starttag(self, tag, attrs):
        self.currentLevel += 1
        #to be inside tab content : <pre class="js-tab-content">
        if tag == 'pre' and len(attrs) == 1 and attrs[0][0] == "class" and attrs[0][1]=="js-tab-content":
            self.tabContentLevel = self.currentLevel
        if self.tabContentLevel and tag=="span": 
            #get chords <span>Dm</span>
            self.inChord = True
    def handle_data(self, data):
        if self.tabContentLevel: 
            if "capo" in data.lower():
                self.capo = self.readCapoPosition(data)
            if "tuning" in data.lower():
                self.tuning = self.readTuning(data)
        if self.inChord:
            self.chords.append(data)
    def handle_endtag(self, tag):
        if self.currentLevel == self.tabContentLevel:
            self.tabContentLevel = None
        self.currentLevel -= 1
        self.inChord = False
        
    def readCapoPosition(self, data):
        for line in data.split("\n"):
            if "capo" in line.lower():
                #ignoring capos in roman numeration for now. in english it may be really hard to destinguish between "I" (me) and "I" (one)
                match = re.match(".*([0-9]+).*", line)
                if match:
                    return match.groups(0)[0]
                return None
    def readTuning(self, data):
        for line in data.split("\n"):
            if "tuning" in line.lower():
                #print line
                #get a way to parse the tuning
                return None
        