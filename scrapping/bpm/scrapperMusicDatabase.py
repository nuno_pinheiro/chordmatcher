print "here"

import requests
from HTMLParser import HTMLParser


class Scrapper:
    def scrap(self):
        parameters = {"search" : "what goes around"}
        data = requests.post("https://www.cs.ubc.ca/~Davet/music/search/basic.cgi", parameters).text
        parser = MyHTMLParser()
        parser.feed(data)


class MyHTMLParser(HTMLParser):
    rowCount = 0
    columnData = ""
    def handle_starttag(self, tag, attrs):
        if tag == "tr":
            self.rowCount += 1
        if self.rowCount > 0 and tag == "a":
            href = attrs[0][1]
            if attrs[0][1].startswith(".."):
                self.link = href
    def handle_endtag(self, tag):
        if tag == "tr":
            self.rowCount -= 1
        if tag == "td":
            #print self.columnData
            self.columnData = ""

            
    def handle_data(self, data):
        if self.rowCount > 0 :
            self.columnData += data
        
Scrapper().scrap()
