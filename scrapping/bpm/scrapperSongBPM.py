import commands
from HTMLParser import HTMLParser
from collections import defaultdict

#This is a really sloppy algorithm: we are not really checking the results, and we use a "majority" approach to get the right bpm
class Scrapper:
    def scrap(self, author, song):
        url = "curl  https://songbpm.com/{0}/{1}".format(author, song)
        data = commands.getstatusoutput(url)[1]
        parser = MyHTMLParser()
        parser.feed(data)
        counts = defaultdict(int)
        for bpm in parser.bpms:
            counts[bpm] += 1
        
        best = None
        bestValue = 0
        for key in counts.keys():
            if counts[key] > bestValue:
                best = key
                bestValue = counts[key]
        
        return best

class MyHTMLParser(HTMLParser):
    
    def __init__(self):
        HTMLParser.__init__(self)
        self.bpms = []
        self.found = False    
    def handle_starttag(self, tag, attrs):
        #We are searching for <div class="number">
        if tag == "div" and len(attrs) == 1 and attrs[0][0] == "class" and attrs[0][1]=="number":  
            self.found = True
    def handle_endtag(self, tag):
        self.found = False
            
    def handle_data(self, data):
        if self.found and data.isdigit():
            self.bpms.append(data)

