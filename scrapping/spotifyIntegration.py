from pymongo import MongoClient
import requests
import json
from threading import Thread
from Queue import Queue

client = MongoClient()
db = client.chordmatcher
queue = Queue()

def processSongs(queue):
    while True:
        song = queue.get(True)
        if "popularity" in song.keys():
            continue
        author = song['author']
        songName = song['song']
        url = "https://api.spotify.com/v1/search?q=artist:{0}%20track:{1}\&type=track".format(author, songName)
        popularity = 0
        image = ""
        spotify_id = ""
        result = requests.get(url)
        if result.status_code == 200:
            jsonResult = json.loads(result.text)
            items = jsonResult['tracks']['items']
            if len(items) > 0:
                spotifySong = items[0]
                popularity = spotifySong['popularity']
                spotify_id = spotifySong['id']
                if "album" in spotifySong.keys():
                    images = spotifySong['album']['images']
                    if len(image) > 0:
                        image = images[0]['url']
        else:
            print "error during access to song " + str(song['_id']) + " error: " + str(result.status_code)
            continue
        song['popularity'] = popularity
        song['spotify_id'] = spotify_id
        song['image'] = image
        db.songs.update({"_id" : song["_id"]}, song)

for i in range(0, 5):
    thread = Thread(target = processSongs, args = [queue])
    thread.start()


for song in db.songs.find().batch_size(500):
    queue.put_nowait(song)
