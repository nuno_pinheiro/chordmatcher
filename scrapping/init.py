import songs.scrapperUltimateGuitar as songsModule
import bpm.scrapperSongBPM as bpmModule
import chords.scrapperUltimateGuitar as chordsModule
from chordCleaner import cleanChord
import patternfinder.finder as patternfinder
from threading import Thread
from pymongo import MongoClient
from Queue import Queue
from sets import Set
import sys
reload(sys)
sys.setdefaultencoding("utf-8")
#queue is a blocking thread safe queue. We will be scrapping songs, send them to the queue, and the songs will be processed and sent to mongo
songs = Queue()
scrapper =songsModule.Scrapper()
thread = Thread(target = scrapper.scrap, args = [songs])
thread.start()

client = MongoClient()
db = client.chordmatcher
def process(songs):
    #client = MongoClient("mongodb://default:zsLrUHRbAMeJeC8uwKXh@ds013569.mlab.com:13569/chordmatcher")
    while(True):
        #blocking get
        song = songs.get(True)
        author = song['author'].replace(" ", "+")
        title = song['song'].replace(" ", "+")
        #song['bpm'] = bpmModule.Scrapper().scrap(author, title)
        chordAnalysis = chordsModule.Scrapper().scrap(song['url'])
        song['chords'] = chordAnalysis['chords']
        if 'capo' in chordAnalysis:
            song['capo'] = chordAnalysis['capo']
        song['patterns'] = patternfinder.detectPattern(song['chords'])
        chords = readChordsFromPatterns(song['patterns'])
        song['allChords'] = [cleanChord(chord) for chord in chords]
        song['chords_count'] = len(song['allChords'].split(","))
        #Add patterns as string to perform perfect match indexing
        song['patternsString'] = patternfinder.patternsListAsString(song['patterns'])
        for pattern in song['patterns']:
            pattern['pattern'] = patternfinder.getPatternAsString(pattern['pattern'])

        db.songs.insert_one(song)

threads = []

for i in range(0,200):
    thread = Thread(target = process, args = [songs])
    thread.start()
    threads.append(thread)

def readChordsFromPatterns(patterns):
    chords = Set()
    for pattern in patterns:
        for chord in pattern['pattern'].split(','):
            chords.add(chord.strip())
    sortedList = ",".join(sorted(list(chords)))
    return str(sortedList)
