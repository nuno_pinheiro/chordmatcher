from pymongo import MongoClient

client = MongoClient()
db = client.chordmatcher
#setup indexes

db.songs.create_index([("patternsString", 1)])
db.songs.create_index([("allChords", 1), ("popularity", -1) ])
db.songs.create_index([("chords_count", 1), ("allChords", 1), ("popularity", -1) ])
db.songs.create_index([("url", 1)])
db.songs.create_index( [ ("author", "text") , ("song" , "text")] )
