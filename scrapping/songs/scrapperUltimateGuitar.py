from http.helper import get
from HTMLParser import HTMLParser
from threading import Thread
from pymongo import MongoClient
import re

class Scrapper:
    def scrap(self, songs):
        for topPage in self.bandsTopPages():
            thread = Thread(target = self.processTopPages, args = [topPage, songs])
            thread.start()
    def processTopPages(self, topPage, songs):
        client = MongoClient()
        db = client.chordmatcher
        i = 1
        foundSomething = True
        while foundSomething:
            foundSomething = False
            link = "https://www.ultimate-guitar.com/bands/" + topPage + str(i) + ".htm"
            data = get(link)
            for line in data.split("\n"):
                bandPage = re.match(".*(/tabs/.*_tabs.htm).*", line)
                if bandPage:
                    self.processBandPage(songs, bandPage.groups()[0], db)
                    foundSomething = True
            i+=1

    def processBandPage(self, songs, bandPage, database):
        bandPagePath = "https://www.ultimate-guitar.com" + bandPage
        data = get(bandPagePath).encode('utf-8')
        parser = MyHTMLParser(songs, database)
        parser.feed(data)
        if parser.nextPage:
            self.processBandPage(songs, parser.nextPage, database)

    def bandsTopPages(self):
        bandsTopPages = []
        for i in 'abcdefghijklmnopqrstuvwxyz':
            bandsTopPages.append(i)
        bandsTopPages.append("0-9")
        return bandsTopPages

class MyHTMLParser(HTMLParser):
    def __init__(self, songs, database):
        HTMLParser.__init__(self)
        self.songs = songs
        self.nextPage = None
        self.pageReference = None
        self.database = database

    def handle_starttag(self, tag, attrs):
        if tag == "a" and len(attrs) == 2:
            #possible page reference
            #<a href="/tabs/x_japan_tabs2.htm" class="ys">Next </a>
            self.pageReference = attrs[0][1]
        #We are searching for <a href="https://tabs.ultimate-guitar.com/j/justin_bieber/love_yourself_crd.htm">
        if tag == "a" and len(attrs) == 1:
            splitted = attrs[0][1].split("/")
            if len(splitted) == 6 and splitted[5] != "":
                songURL = attrs[0][1]
                song = splitted[5]
                #remove .htm from song name
                song = song[0:len(song) -4]
                #print song
                songAndType = self.getType(song)
                if not songAndType:
                    return
                type = songAndType[1]
                if type != "chords":
                    return
                if self.database.songs.find({"url" : songURL}).count() > 0:
                    #Skip already procesed songs
                    return
                song = songAndType[0]
                song = song.replace("_", " ");
                #check if song title contains a version string
                match =  re.search("(.*)ver[0-9]+(.*)", song)
                if match:
                    groups = match.groups()
                    song = groups[0] + groups[1]
                    #if search:
                        #print search.groups()
                author = splitted[4].replace("_", " ")
                self.songs.put_nowait({"author" : author, "song" : song, "url" : songURL})
    def getType(self, song):
        splitted = song.split("_")
        found = False
        index = len(splitted) - 1
        strToRemove = "";
        # This split is not very good because it will avoid any song which has album or ukulele in the name
        # To be properly performed we should check the song type column, but it is hard with this parser
        if "album" in splitted:
            found = "album"
        if "ukulele" in splitted:
            found = "ukulele"
        while not found and index >= 0:
            type = splitted[index]
            if type == "crd":
                found = "chords"
            elif type == "tab":
                found = "tab"
            elif type == "btab":
                #bass tab
                found = "btab"
            elif type == "pro":
                #guitar pro
                found = "pro"
            elif type == "lesson":
                #video lesson
                found = "lesso"
            strToRemove = "_" + splitted[index] + strToRemove
            index = index - 1
        if found:
            #We want to remove the type part
            song = song[0:song.rfind(strToRemove)]
            return [song, found]
    def handle_endtag(self, tag):
        if self.pageReference:
            self.pageReference = None
    def handle_data(self, data):
        if self.pageReference and data.startswith("Next"):
            self.nextPage = self.pageReference
