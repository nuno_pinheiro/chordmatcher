import requests
from HTMLParser import HTMLParser

import re

class Scrapper:
    def scrap(self):
        #data = requests.get("https://www.ultimate-guitar.com/top/top100.htm").text
        data = requests.get("https://www.ultimate-guitar.com/tabs").text
        parser = MyHTMLParser()
        parser.feed(data)
        return parser.songs

class MyHTMLParser(HTMLParser):
    songs = []
    found = False
    def handle_starttag(self, tag, attrs):
        #We are searching for <a href="https://tabs.ultimate-guitar.com/j/justin_bieber/love_yourself_crd.htm">
        if tag == "a" and len(attrs) == 1:  
            splitted = attrs[0][1].split("/")
            if len(splitted) == 6:
                song = splitted[5]
                #remove .htm from song name
                song = song[0:len(song) -4]
                songAndType = self.getType(song)
                type = songAndType[1]
                if type != "chords":
                    return
                song = songAndType[0]
                song = song.replace("_", " ");
                #check if song title contains a version string
                match =  re.search("(.*)ver[0-9]+(.*)", song)
                if match:
                    groups = match.groups()
                    song = groups[0] + groups[1]
                    #if search:
                        #print search.groups()
                author = splitted[4].replace("_", " ")
                self.songs.append({"author" : author, "song" : song, "url" : attrs[0][1]})
    def getType(self, song):
        splitted = song.split("_")
        found = False
        index = len(splitted) - 1
        strToRemove = "";
        while not found and index >= 0:
            type = splitted[index]
            if type == "crd":
                found = "chords"
            elif type == "tab":
                found = "tab"
            strToRemove = "_" + splitted[index] + strToRemove
            index = index - 1
        if found:
            #We want to remove the type part
            song = song[0:song.rfind(strToRemove)]
            return [song, found]
    #def handle_endtag(self, tag):
            
    #def handle_data(self, data):
